package com.noahjutz.composepencil

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource

enum class Chart(
    val text: String,
    val icon: ImageVector,
    val route: String,
) {
    Line("Line Chart", Icons.Default.ShowChart, "lineChart"),
    Bar("Bar Chart", Icons.Default.BarChart, "barChart"),
    Pie("Pie Chart", Icons.Default.PieChart, "PieChart")
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ChartList(
    navigate: (String) -> Unit
) {
    Scaffold(
        topBar = {
            TopBar(
                icon = {
                    Icon(
                        Icons.Default.Insights, // TODO logo
                        null,
                    )
                },
                text = { Text(stringResource(R.string.app_name)) }
            )
        }
    ) {
        Column(Modifier.verticalScroll(rememberScrollState())) {
            for (chart in Chart.values()) {
                ListItem(
                    modifier = Modifier.clickable { navigate(chart.route) },
                    text = { Text(chart.text) },
                    icon = { Icon(chart.icon, null) },
                    trailing = { Icon(Icons.Default.ChevronRight, null) },
                )
            }
        }
    }
}
