package com.noahjutz.composepencil

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.*
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.ProvideTextStyle
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun TopBar(
    text: @Composable () -> Unit,
    icon: @Composable BoxScope.() -> Unit,
) {
    Surface(
        elevation = if (isSystemInDarkTheme()) 0.dp else 4.dp,
    ) {
        Column {
            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Box(Modifier.size(24.dp), content = icon)
                Spacer(Modifier.width(12.dp))
                ProvideTextStyle(typography.h6, content = text)
            }
            if (isSystemInDarkTheme()) Divider(Modifier.fillMaxWidth())
        }
    }
}
