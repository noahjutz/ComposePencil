package com.noahjutz.composepencil

import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.ProvideTextStyle
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp

@Composable
fun SettingItem(
    modifier: Modifier = Modifier,
    icon: @Composable BoxScope.() -> Unit,
    title: @Composable () -> Unit,
    content: @Composable () -> Unit,
) {
    Row(
        modifier
            .fillMaxSize()
            .padding(top = 16.dp, start = 16.dp, end = 16.dp)
    ) {
        Box(Modifier.size(24.dp), content = icon)
        Spacer(Modifier.width(32.dp))
        Column(Modifier.weight(1f)) {
            Box(Modifier.height(24.dp), contentAlignment = Alignment.CenterStart) {
                ProvideTextStyle(
                    typography.body1.copy(fontWeight = FontWeight.Bold),
                    content = title
                )
            }
            Spacer(Modifier.height(12.dp))
            ProvideTextStyle(typography.body1, content = content)
        }
    }
}
