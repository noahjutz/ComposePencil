package com.noahjutz.composepencil

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController

@Composable
fun NavGraph() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = "chartList") {
        composable(route = "chartList") {
            ChartList(navigate = { navController.navigate(it) })
        }
        composable(route = "lineChart") {
            LineChartDisplay(popBackStack = { navController.popBackStack() })
        }
        composable(route = "barChart") {
        }
        composable(route = "pieChart") {
        }
    }
}
