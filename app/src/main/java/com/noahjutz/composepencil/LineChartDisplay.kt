package com.noahjutz.composepencil

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.CropFree
import androidx.compose.material.icons.filled.Source
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.abs
import kotlin.math.roundToInt
import kotlin.time.Duration
import kotlin.time.ExperimentalTime

@OptIn(ExperimentalMaterialApi::class, kotlin.time.ExperimentalTime::class)
@Composable
fun LineChartDisplay(
    popBackStack: () -> Unit,
) {
    Scaffold(
        topBar = {
            TopBar(
                text = { Text("Line Chart") },
                icon = {
                    IconButton(onClick = popBackStack) {
                        Icon(Icons.Default.ArrowBack, "Back")
                    }
                }
            )
        }
    ) {
        Column(Modifier.verticalScroll(rememberScrollState())) {
            val (widthFraction, setWidthFraction) = remember { mutableStateOf(1f) }
            val (heightDp, setHeightDp) = remember { mutableStateOf(300.dp) }

            val (yMin, setYMin) = remember { mutableStateOf(-30) }
            val (yMax, setYMax) = remember { mutableStateOf(50) }
            val (nX, setNX) = remember { mutableStateOf(1000) }
            val dataset by remember(yMin, yMax, nX) {
                mutableStateOf(randomDataset(yMin, yMax, nX))
            }

            val colors = colors

            Card(Modifier.padding(16.dp)) {
                Column(
                    Modifier
                        .fillMaxWidth()
                        .padding(16.dp),
                ) {
                    Text("Preview", style = typography.h5)
                    Spacer(Modifier.height(16.dp))
                    Box(
                        Modifier
                            .fillMaxWidth()
                            .height(300.dp),
                        contentAlignment = Alignment.Center
                    ) {
                        LineChart(
                            modifier = Modifier
                                .fillMaxWidth(widthFraction)
                                .height(heightDp),
                            xString = ::getXString,
                            yString = ::getYString,
                            xAxisName = "Date [dd/mm]",
                            yAxisName = "Duration [h]"
                        ) {
                            lineChart(
                                data = dataset,
                                color = colors.primary
                            )
                            lineChart(
                                data = dataset.map { it.copy(y = it.y / 2) },
                                color = colors.onSurface.copy(alpha = 0.12f)
                            )
                        }
                    }
                }
            }
            SettingItem(
                title = { Text("Dataset") },
                icon = { Icon(Icons.Default.Source, null) },
            ) {
                Column {
                    Button(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(50.dp)
                            .padding(bottom = 8.dp),
                        onClick = {
                            setYMin((-200..200).random())
                            setYMax((400..600).random())
                            setNX((30..3000).random())
                        }
                    ) {
                        Text("Randomize yMin, yMax, nX")
                    }
                    TextField(
                        modifier = Modifier.fillMaxWidth(),
                        value = nX.toString(),
                        onValueChange = { setNX(it.toIntOrNull() ?: 30) },
                        label = { Text("nX") }
                    )
                }
            }
            SettingItem(
                title = { Text("Size") },
                icon = { Icon(Icons.Default.CropFree, null) }
            ) {
                Column {
                    Text("Width: ${(widthFraction * 100).toInt()}%")
                    Slider(
                        value = widthFraction,
                        onValueChange = setWidthFraction,
                        valueRange = 0.1f..1f
                    )
                    Text("Height: ${heightDp.value.roundToInt()}dp")
                    Slider(
                        value = heightDp.value,
                        onValueChange = { setHeightDp(it.dp) },
                        valueRange = 100f..300f,
                    )
                }
            }
            Spacer(Modifier.height(32.dp))
        }
    }
}

@OptIn(ExperimentalStdlibApi::class)
fun randomDataset(
    yMin: Int,
    yMax: Int,
    nX: Int,
): List<DataPoint> {
    return buildList(nX) {
        val yRange = abs(yMin) + abs(yMax)
        for (i in 0..nX) {
            add(
                DataPoint(
                    x = i,
                    y = if (i == 0) (yMin..yMax).random().toFloat()
                    else this[i - 1].y + (-(yRange / 5)..(yRange / 5)).random()
                )
            )
        }
    }
}

@OptIn(ExperimentalTime::class)
fun getXString(x: Int): String {
    val date = Date(Calendar.getInstance().timeInMillis + Duration.days(x).inWholeMilliseconds)
    val format = SimpleDateFormat("dd/MM", Locale.getDefault())
    return format.format(date)
}

@OptIn(ExperimentalTime::class)
fun getYString(y: Int): String {
    val duration = Duration.minutes(y)
    val hours = duration.toDouble(TimeUnit.HOURS)
    return "%.1f".format(hours) + "h"
}
