package com.noahjutz.composepencil

import android.graphics.Paint
import android.graphics.Typeface
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.drawscope.translate
import androidx.compose.ui.graphics.nativeCanvas
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.*

enum class TextAnchor {
    TopLeft,
    TopCenter,
    TopRight,
    CenterLeft,
    Center,
    CenterRight,
    BottomLeft,
    BottomCenter,
    BottomRight,
}

private fun DrawScope.textPaint(style: TextStyle): Paint {
    check(style.fontSize.isSp) { "TextStyle passed to textPaint must contain fontSize property as Sp unit" }
    return Paint().apply {
        isAntiAlias = true
        color = style.color.toArgb()
        textSize = style.fontSize.toPx()
        alpha = (style.color.alpha * 255f).toInt()
        typeface = when (style.fontFamily) {
            FontFamily.Default -> Typeface.DEFAULT
            FontFamily.Monospace -> Typeface.MONOSPACE
            FontFamily.Cursive -> Typeface.DEFAULT // TODO
            FontFamily.SansSerif -> Typeface.SANS_SERIF
            FontFamily.Serif -> Typeface.SERIF
            else -> Typeface.DEFAULT
        }
    }
}

internal fun DrawScope.measureText(text: String, style: TextStyle): Size {
    val paint = textPaint(style)
    return Size(
        width = paint.measureText(text),
        height = paint.fontMetrics.let { it.descent - it.ascent }
    )
}

internal fun DrawScope.drawText(
    text: String,
    offset: Offset = center,
    style: TextStyle,
    textAnchor: TextAnchor = TextAnchor.TopLeft
) {
    val paint = textPaint(style)
    val textBounds = measureText(text, style)
    val translateOffset = when (textAnchor) {
        TextAnchor.TopLeft -> Offset(0f, textBounds.height)
        TextAnchor.TopCenter -> Offset(-textBounds.width / 2, textBounds.height)
        TextAnchor.TopRight -> Offset(-textBounds.width, textBounds.height)
        TextAnchor.CenterLeft -> Offset(0f, textBounds.height / 2)
        TextAnchor.Center -> Offset(-textBounds.width / 2, textBounds.height / 2)
        TextAnchor.CenterRight -> Offset(-textBounds.width, textBounds.height / 2)
        TextAnchor.BottomLeft -> Offset(0f, 0f)
        TextAnchor.BottomCenter -> Offset(-textBounds.width / 2, 0f)
        TextAnchor.BottomRight -> Offset(-textBounds.width, 0f)
    }
    translate(left = translateOffset.x, top = translateOffset.y) {
        drawIntoCanvas {
            it.nativeCanvas.drawText(text, offset.x, offset.y, paint)
        }
    }
}
