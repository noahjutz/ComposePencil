package com.noahjutz.composepencil

import androidx.compose.foundation.Canvas
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.DrawScope
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.inset
import androidx.compose.ui.graphics.drawscope.withTransform
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

data class VerticalLine(
    val x: Int,
    val color: Color,
    val width: Dp,
)

data class HorizontalLine(
    val y: Float,
    val color: Color,
    val width: Dp,
)

data class PlotLine(
    val data: List<DataPoint>,
    val color: Color,
)

data class DataPoint(
    val x: Int,
    val y: Float,
)

class LineChartBuilder {
    val charts = mutableListOf<PlotLine>()
    val verticalLines = mutableListOf<VerticalLine>()
    val horizontalLines = mutableListOf<HorizontalLine>()

    fun lineChart(data: List<DataPoint>, color: Color) {
        charts.add(PlotLine(data, color))
    }

    fun verticalLine(x: Int, color: Color, width: Dp = Dp.Hairline) {
        verticalLines.add(VerticalLine(x, color, width))
    }

    fun horizontalLine(y: Float, color: Color, width: Dp = Dp.Hairline) {
        horizontalLines.add(HorizontalLine(y, color, width))
    }
}

@Composable
fun LineChart(
    modifier: Modifier,
    xString: (Int) -> String = { it.toString() },
    yString: (Int) -> String = { it.toString() },
    xAxisName: String? = null,
    yAxisName: String? = null,
    labelStyle: TextStyle = TextStyle(
        fontSize = 12.sp,
        color = colors.onSurface.copy(alpha = 0.5f)
    ),
    axisNameStyle: TextStyle = TextStyle(
        fontSize = 12.sp,
        color = colors.onSurface.copy(alpha = 0.5f)
    ),
    data: LineChartBuilder.() -> Unit,
) {
    val builder = LineChartBuilder()
    builder.data()

    LineChartImpl(
        modifier = modifier,
        plots = builder.charts,
        xString = xString,
        yString = yString,
        xName = xAxisName,
        yName = yAxisName,
        labelStyle = labelStyle,
        axisNameStyle = axisNameStyle,
    )
}

@Composable
fun LineChart(
    modifier: Modifier,
    data: List<DataPoint>,
    color: Color = colors.primary,
    xAxisName: String? = null,
    yAxisName: String? = null,
    labelStyle: TextStyle = TextStyle(
        fontSize = 12.sp,
        color = colors.onSurface.copy(alpha = 0.5f)
    ),
    axisNameStyle: TextStyle = TextStyle(
        fontSize = 12.sp,
        color = colors.onSurface.copy(alpha = 0.5f)
    ),
    xString: (Int) -> String = { it.toString() },
    yString: (Int) -> String = { it.toString() }
) {
    LineChartImpl(
        modifier = modifier,
        plots = listOf(PlotLine(data, color)),
        xString = xString,
        yString = yString,
        xName = xAxisName,
        yName = yAxisName,
        labelStyle = labelStyle,
        axisNameStyle = axisNameStyle,
    )
}

@Composable
private fun LineChartImpl(
    modifier: Modifier,
    plots: List<PlotLine>,
    xString: (Int) -> String = { it.toString() },
    yString: (Int) -> String = { it.toString() },
    xName: String? = null,
    yName: String? = null,
    labelStyle: TextStyle,
    axisNameStyle: TextStyle,
) {
    val colors = colors

    Canvas(modifier) {
        val datasets = plots.map { it.data }
        val xMin = datasets.minOf { it.minOf { it.x } }
        val xMax = datasets.maxOf { it.maxOf { it.x } }
        val yMin = datasets.minOf { it.minOf { it.y } }
        val yMax = datasets.maxOf { it.maxOf { it.y } }

        val yLabels = getYLabels(
            min = yMin.toInt(),
            max = yMax.toInt(),
            labelTransform = yString,
            minGapPx = 32.dp.toPx(),
            textStyle = labelStyle,
        )

        val xLabels = getXLabels(
            min = xMin,
            max = xMax,
            labelTransform = xString,
            minGapPx = 16.dp.toPx(),
            textStyle = labelStyle,
        )

        val xNameHeight = xName?.let { measureText(it, axisNameStyle).height } ?: 0f
        val xLabelHeight =
            if (xLabels.size <= 1) listOf(xMin, xMax).maxOf {
                measureText(yString(it), style = labelStyle).height
            }
            else xLabels.maxOf {
                measureText(yString(it), style = labelStyle).height
            }
        val xAxisHeight = 8.dp.toPx() + xLabelHeight + xNameHeight

        val yLabelWidth =
            if (yLabels.size <= 1) listOf(yMin, yMax).maxOf {
                measureText(yString(it.toInt()), style = labelStyle).width
            }
            else yLabels.maxOf {
                measureText(yString(it), style = labelStyle).width
            }
        val yAxisWidth = 8.dp.toPx() + yLabelWidth
        val yNameHeight =
            yName?.let { measureText(it, axisNameStyle).height + 4.dp.toPx() } ?: 0f

        // X-Axis
        inset(top = size.height - xAxisHeight, bottom = 0f, left = 0f, right = yAxisWidth) {
            drawPath(
                Path().apply {
                    // Baseline
                    moveTo(0f, 1.dp.toPx())
                    lineTo(size.width, 1.dp.toPx())
                    // Ticks
                    if (xLabels.size > 1) {
                        for (x in xLabels) {
                            val xOffset = ((x - xMin) / (xMax - xMin).toFloat()) * size.width

                            moveTo(xOffset, 4.dp.toPx())
                            lineTo(xOffset, 0f)
                        }
                    } else {
                        moveTo(0f, 4.dp.toPx())
                        lineTo(0f, 0f)
                        moveTo(size.width, 4.dp.toPx())
                        lineTo(size.width, 0f)
                    }
                },
                color = colors.onSurface.copy(alpha = 0.12f),
                style = Stroke(width = 2.dp.toPx())
            )
            // Labels
            if (xLabels.size > 1) {
                for (x in xLabels) {
                    val xOffset = ((x - xMin) / (xMax - xMin).toFloat()) * size.width

                    drawText(
                        text = xString(x),
                        style = labelStyle,
                        offset = Offset(xOffset, 8.dp.toPx()),
                        textAnchor = TextAnchor.TopCenter
                    )
                }
            } else {
                drawText(
                    text = xString(xMin),
                    style = labelStyle,
                    offset = Offset(0f, 8.dp.toPx()),
                    textAnchor = TextAnchor.TopLeft
                )
                drawText(
                    text = xString(xMax),
                    style = labelStyle,
                    offset = Offset(size.width, 8.dp.toPx()),
                    textAnchor = TextAnchor.TopRight
                )
            }
            // Axis name
            if (xName != null) {
                drawText(
                    text = xName,
                    style = axisNameStyle,
                    offset = Offset(0f, size.height),
                    textAnchor = TextAnchor.BottomLeft
                )
            }
        }

        // Y-Axis
        inset(top = yNameHeight, bottom = xAxisHeight, left = size.width - yAxisWidth, right = 0f) {
            // Labels
            if (yLabels.size > 1) {
                for (y in yLabels) {
                    val yOffset = (1 - (y - yMin) / (yMax - yMin)) * size.height
                    drawText(
                        text = yString(y),
                        style = labelStyle,
                        offset = Offset(8.dp.toPx(), yOffset),
                        textAnchor = TextAnchor.CenterLeft
                    )
                }
            } else {
                drawText(
                    text = yString(yMin.toInt()),
                    style = labelStyle,
                    offset = Offset(8.dp.toPx(), size.height),
                    textAnchor = TextAnchor.BottomLeft
                )
                drawText(
                    text = yString(yMax.toInt()),
                    style = labelStyle,
                    offset = Offset(8.dp.toPx(), 0f),
                    textAnchor = TextAnchor.TopLeft
                )
            }
        }

        // Content
        withTransform({
            inset(top = yNameHeight, bottom = xAxisHeight, left = 0f, right = yAxisWidth)
            clipRect()
        }) {
            // Guidelines
            if (yLabels.size > 1) {
                for (y in yLabels) {
                    val yOffset = (1 - (y - yMin) / (yMax - yMin)) * size.height

                    drawLine(
                        color = colors.onSurface.copy(alpha = 0.12f),
                        start = Offset(0f, yOffset),
                        end = Offset(size.width, yOffset)
                    )
                }
            } else {
                drawLine(
                    color = colors.onSurface.copy(alpha = 0.12f),
                    start = Offset(0f, 0f),
                    end = Offset(size.width, 0f),
                    strokeWidth = 1f,
                )
            }
            // Chart
            for ((data, color) in plots) {
                drawPath(
                    path = Path().apply {
                        data.forEachIndexed { i, p ->
                            val x = (p.x - xMin) / (xMax - xMin).toFloat() * size.width
                            val y = (1 - (p.y - yMin) / (yMax - yMin)) * size.height

                            if (i == 0) moveTo(x, y)
                            else lineTo(x, y)
                        }
                    },
                    color = color,
                    style = Stroke(2.dp.toPx(), cap = StrokeCap.Round)
                )
            }
        }

        // Y Axis Name
        if (yName != null) {
            drawText(
                text = yName,
                offset = Offset(size.width, 0f),
                style = axisNameStyle,
                textAnchor = TextAnchor.TopRight
            )
        }
    }
}

tailrec fun DrawScope.getXLabels(
    min: Int,
    max: Int,
    labelTransform: (Int) -> String,
    textStyle: TextStyle,
    minGapPx: Float = 0f,
    interval: Int = 1
): List<Int> {
    val labels = (min..max).filter { it % interval == 0 }

    fun Int.toRect(): Rect {
        val x = ((this - min) / (max - min).toFloat()) * size.width
        val width = measureText(labelTransform(this), textStyle).width
        return Rect(Offset(x - width / 2, 0f), Size(width, 0f))
    }

    val rects = mutableListOf(labels.first().toRect())

    labels.drop(1).forEachIndexed { i, label ->
        val previous = rects[i]
        val next = label.toRect()
        rects.add(next)

        val gap = next.left - previous.right

        if (gap < minGapPx) {
            val nextInterval = when (interval.toString()[0].digitToInt()) {
                1 -> interval * 2
                2 -> (interval * 2.5).toInt()
                5 -> interval * 2
                else -> throw NumberFormatException()
            }
            return getXLabels(
                min,
                max,
                labelTransform,
                textStyle,
                minGapPx,
                nextInterval
            )
        }
    }

    val isFirstOutside = rects.first().left < 0f
    val isLastOutside = rects.last().right > size.width

    return labels
        .drop(if (isFirstOutside) 1 else 0)
        .dropLast(if (isLastOutside) 1 else 0)
}

tailrec fun DrawScope.getYLabels(
    min: Int,
    max: Int,
    labelTransform: (Int) -> String,
    textStyle: TextStyle,
    minGapPx: Float = 0f,
    interval: Int = 1
): List<Int> {
    val labels = (min..max).filter { it % interval == 0 }

    fun Int.toRect(): Rect {
        val y = (1 - (this - min) / (max - min).toFloat()) * size.height
        val height = measureText(labelTransform(this), textStyle).height
        return Rect(Offset(0f, y - height / 2), Size(0f, height))
    }

    val rects = mutableListOf(labels.first().toRect())

    labels.drop(1).forEachIndexed { i, label ->
        val previous = rects[i]
        val next = label.toRect()
        rects.add(next)

        val gap = previous.top - next.bottom

        if (gap < minGapPx) {
            val nextInterval = when (interval.toString()[0].digitToInt()) {
                1 -> interval * 2
                2 -> (interval * 2.5).toInt()
                5 -> interval * 2
                else -> throw NumberFormatException()
            }
            return getYLabels(
                min,
                max,
                labelTransform,
                textStyle,
                minGapPx,
                nextInterval
            )
        }
    }

    val isFirstOutside = rects.first().top < 0f
    val isLastOutside = rects.last().bottom > size.height

    return labels
        .drop(if (isFirstOutside) 1 else 0)
        .dropLast(if (isLastOutside) 1 else 0)
}
